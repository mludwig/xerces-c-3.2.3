# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.20

# compile CXX with /usr/bin/c++
CXX_DEFINES = -DHAVE_CONFIG_H=1 -D_FILE_OFFSET_BITS=64 -D_THREAD_SAFE=1

CXX_INCLUDES = -I/home/mludwig/3rdPartySoftware/xerces-c-3.2.3 -I/home/mludwig/3rdPartySoftware/xerces-c-3.2.3/src -I/home/mludwig/3rdPartySoftware/xerces-c-3.2.3/samples

CXX_FLAGS =  -Wall -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wextra -Wformat=2 -Wmissing-declarations -Wno-long-long -Woverlength-strings -Woverloaded-virtual -Wredundant-decls -Wreorder -Wswitch-default -Wunused-variable -Wwrite-strings -Wno-variadic-macros -fstrict-aliasing -msse2 -O3 -DNDEBUG -pthread -std=gnu++14

